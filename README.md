# Geist Watchdog

A lightweight program for polling data from a Geist watchdog and uploading the data to InfluxDB.

## config.json

Used to configure the program. The following properties are supported:

| Property | Description |
|-------------|-----------------------------|
| influxdb_addr | Network address for posting data to InfluxDB. |
| database | Database name to use in the InfluxDB. |
| interval | interval in seconds between polling for data. |

For example:
```
{
    "influxdb_addr": "allxdaq22:8086",
    "database": "aion",
    "interval": 2.0
}
```

## channels.json

A dictionary of device IPs and channels to poll from the watchdog. The channel keys correspond to channel names (as they will appear in InfluxDB) and the values correspond to the path in the payload of data returned from the watchdog device. For example:
```
{
"some.device.1.ip":
	{
		"lab_temp": "data/BF5410ECC9E266C3/entity/0/measurement/0/value",
		"lab_humidity": "data/BF5410ECC9E266C3/entity/0/measurement/1/value"
	},
"some.device.2.ip":
	{
		"apparatus_temp": "data/D8049162E7FCDAC3/entity/0/measurement/0/value",
		"apparatus_humidity": "data/D8049162E7FCDAC3/entity/0/measurement/1/value"
	},
}

```
