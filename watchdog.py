#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 11:55:37 2021

@author: bentine
"""

import requests
import json
import time

"""
Make a measurement of the sensors at the given IP
"""
def make_measurement(ip):
    url = 'http://{ip}/api/dev'.format(ip=ip)
    headers = {'Accept' : 'application/json', 'Content-Type' : 'application/json'}
    data = {'cmd': 'get'}
    r = requests.post(url, json=data, headers=headers)
    return json.loads(r.text)

def load_config():
    with open('config.json') as json_file:
        return json.load(json_file)

def load_device_channels():
    with open('channels.json') as json_file:
        return json.load(json_file)

def send_to_db(config, channel, value):
    content = "{channel} value={value}".format(
            channel=channel,
            value=value
            )
    url = "http://{influxdb}/write?db={database}".format(
            influxdb=config['influxdb_addr'],
            database=config['database']
            )
    r = requests.post(url, data=content)
    print('{0}: {1}'.format(url, content))
    print(r.text)

def log_channels(payload, config, channels):
    for i, (k, v) in enumerate(channels.items()):
        parts = v.split("/")
        element = payload
        for p in parts:
            element = element[p]
        value = element
        send_to_db(config, k, value)

def main():
    config = load_config()
    device_channels = load_device_channels()
    delay = float(config['interval'])
    
    try:
        while True:
            for i, (device, channels) in enumerate(device_channels.items()):
                payload = make_measurement(device)
                log_channels(payload, config, channels)
            time.sleep(delay)
    except KeyboardInterrupt:
        print('Closing monitor.')
    
if __name__ == "__main__":
    main()
        
    